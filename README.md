# GitLab and Dataform Integration Demo

## Overview
This demo showcases the integration of GitLab with Dataform, using a SQLX file to analyze daily top Google Search terms from a public BigQuery dataset. It demonstrates how Dataform can be used for creating sophisticated data models and how GitLab facilitates version control and collaboration in data engineering projects.

## Prerequisites
- Access to GitLab (either GitLab.com or a self-hosted instance).
- A Dataform account/project set up and connected to a Google BigQuery project.
- Basic knowledge of SQL and Git operations.

## File Description
- `daily_top_google_search_terms.sqlx`: This SQLX file contains a Dataform configuration and SQL query to create a view showcasing the top Google Search terms daily, using the public BigQuery dataset.